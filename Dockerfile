FROM ruby:2.5.1-slim

RUN apt-get update -qq && apt-get install -y gnupg --no-install-recommends \
    dirmngr \
    build-essential \
    libpq-dev \
    nodejs \
    git-core \
    ssh \
    wget

RUN gpg --batch --keyserver ha.pool.sks-keyservers.net --recv-keys B97B0AFCAA1A47F044F244A07FCC7D46ACCC4CF8 \
    && echo 'deb http://apt.postgresql.org/pub/repos/apt/ bionic-pgdg main' > /etc/apt/sources.list.d/pgdg.list
RUN wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -

RUN apt-get update -qq && apt-get install -y postgresql-client-11

RUN mkdir /nbp-startup-backend
WORKDIR /nbp-startup-backend
ADD Gemfile /nbp-startup-backend/Gemfile
ADD Gemfile.lock /nbp-startup-backend/Gemfile.lock

ADD . /nbp-startup-backend
