# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.1'

gem 'rails', '~> 5.2.1'
gem 'puma', '~> 3.11'

gem 'pg', '~> 1.1.3'

gem 'redis', '~> 4.0.3'
gem 'sidekiq', '~> 5.2.3'
gem 'sidekiq-cron', '~> 1.0.4'
# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false

gem 'rack-cors', '~> 1.0.2'

gem 'sequel-rails', '~> 1.0.1'
gem 'sequel', '~> 5.14'

gem 'jsonapi-rails', github: 'jsonapi-rb/jsonapi-rails'

gem 'dry-validation', '~> 0.12.2', github: 'dry-rb/dry-validation'
gem 'dry-monads', '~> 1.1'

gem 'faraday', '~> 0.15.4'
gem 'knock', '~> 2.1'

group :development, :test do
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'dotenv-rails', '~> 2.5'
  gem 'ffaker', '~> 2.9'
  gem 'factory_bot_rails', '~> 4.11'
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'sequel-annotate', require: false
end

group :test do
  gem 'database_cleaner', '~> 1.7', github: 'DatabaseCleaner/database_cleaner'
  gem 'rspec_api_documentation', '~> 6.1'
  gem 'rspec-rails', '~> 3.8'
  gem 'jsonapi-rspec', require: false
  gem 'vcr', '~> 4.0'
  gem 'webmock', '~> 3.4.2'
  gem 'timecop', '~> 0.9.1'
end

group :production do
  gem 'rack-attack', '~> 5.4.2'
end
