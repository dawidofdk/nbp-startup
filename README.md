## Requirements

* [Docker & Docker compose](https://docs.docker.com/compose/install/)

## Install

Install Docker.

1. Make sure you're in the latest master
2. Create .env.development
3. PROFIT! You have api running on `localhost:3001` if you visit it you should see landing page saying that you're on rails!


### Documentation:
Documentation not finished, but some part is available on [nbpstartup.docs.apiary.io](https://nbpstartup.docs.apiary.io/#reference/rates)

#### Annotate models

```
rake annotate
```
