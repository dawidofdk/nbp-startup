# frozen_string_literal: true

class Api::V1::ApplicationController < ::ApplicationController
  include Knock::Authenticable

  before_action :authenticate_user

  def jsonapi_class
    Hash.new { |h, k| h[k] = "Api::V1::#{k}Serializer".safe_constantize }
  end

  def underscored_include_directives
    include_directives.deep_transform_keys { |key| key.to_s.underscore.to_sym }
  end

  def resolve_action
    [
      self.class.name.deconstantize,
      controller_name.capitalize,
      "#{action_name}_action".classify
    ].join("::").constantize
  end

  def api_action(**extra_args)
    resolve_action
      .new(extra_args.reverse_merge(current_user: current_user))
      .call(params.to_unsafe_h) do |result|
        yield(result)
      end
  end
end
