# frozen_string_literal: true

class Api::V1::RatesController < Api::V1::ApplicationController
  def index
    render jsonapi: DaysFinder.new(params: params).call
  end
end
