# frozen_string_literal: true

class DaysFinder
  def initialize(params: nil)
    @params = params
  end

  def call
    scope = Day.order(:date)
    scope = apply_range(scope, params) if params.key?(:from_date) && params.key?(:till_date)

    scope.all
  end

  private

  attr_reader :params

  def apply_range(scope, params)
    from_date = Date.parse(params[:from_date])
    till_date = Date.parse(params[:till_date])
    count_days_query = scope.where(date: from_date..till_date)

    count_days = (from_date..till_date).count
    if count_days != count_days_query.count
      Sequel::Model.db.transaction do
        %w[usd eur].each do |currency|
          NbpService.new(currency, from_date, till_date).call
        end
      end
    end

    count_days_query
  end
end
