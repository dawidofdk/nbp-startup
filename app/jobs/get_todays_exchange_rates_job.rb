# frozen_string_literal: true

class GetTodaysExchangeRatesJob < ActiveJob::Base
  queue_as :cron

  def perform
    %w[usd eur].each do |currency|
      NbpService.new(currency).call
    end
  end
end
