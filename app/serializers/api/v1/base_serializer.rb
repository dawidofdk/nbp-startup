# frozen_string_literal: true

class Api::V1::BaseSerializer < JSONAPI::Serializable::Resource
  extend JSONAPI::Serializable::Resource::ConditionalFields
  extend JSONAPI::Serializable::Resource::KeyFormat

  key_format ->(key) { key.to_s.camelize(:lower) }
end
