# frozen_string_literal: true

class Api::V1::CurrencySerializer < Api::V1::BaseSerializer
  type 'currencies'

  attributes :symbol,
             :bid,
             :ask,
             :created_at

  has_one :day
end
