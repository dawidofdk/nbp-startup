# frozen_string_literal: true

class Api::V1::DaySerializer < Api::V1::BaseSerializer
  type 'days'

  attributes :date
  has_many :currencies
end
