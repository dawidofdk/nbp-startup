# frozen_string_literal: true

class NbpService
  API_URL = 'http://api.nbp.pl/api/exchangerates'

  def initialize(currency = 'usd', from_date = Date.today, till_date = nil)
    @from_date, @till_date = parse_dates(from_date, till_date)
    @currency = currency
  end

  def call
    save(exchange_rates.dig('rates'))
  end

  private

  attr_reader :from_date, :till_date, :currency

  def parse_dates(from_date, till_date)
    parse = lambda do |date|
      return nil if date.nil?

      date.is_a?(Date) ? date : Date.parse(date)
    end

    [from_date, till_date].map(&parse)
  rescue ArgumentError => e
    raise e.message
  end

  def exchange_rates
    rates_url = if till_date.nil?
                  "#{API_URL}/rates/c/#{currency}/#{from_date.strftime('%F')}?format=json"
                else
                  "#{API_URL}/rates/c/#{currency}/#{from_date.strftime('%F')}/#{till_date.strftime('%F')}?format=json"
                end
    rates_resp = Faraday.get(rates_url)
    raise 'Rates for this day is not available' unless rates_resp.status.eql?(200)

    JSON.parse(rates_resp.body)
  end

  def save(rates)
    rates.each do |record|
      day = Day.find_or_create(date: record['effectiveDate'])
      Currency.find_or_create(day: day, ask: record['ask'], bid: record['bid'], symbol: currency) do |c|
        c.created_at = Time.now
      end
    end
  end
end
