# frozen_string_literal: true

class Users::AuthService
  def call(payload)
    auth_token = payload['sub']
    return nil unless auth_token.present?

    user = find_user(auth_token)

    user || create_user(payload)
  end

  private

  def find_user(auth_token)
    User.find(auth_token: auth_token)
  end

  def create_user(payload)
    User.create(
      auth_token: payload['sub'],
      name: param_from_payload(payload, 'name'),
      email: param_from_payload(payload, 'email')
    )
  end

  def param_from_payload(payload, param_name)
    payload["#{ENV.fetch('AUTH0_AUDIENCE')}/#{param_name}"]
  end
end
