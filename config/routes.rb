# frozen_string_literal: true

require 'sidekiq/web'
require 'sidekiq/cron/web'

Rails.application.routes.draw do
  current_api_routes = lambda do
    constraints(id: /\d+/) do
      resources :rates, only: :index
    end
  end

  namespace :api do
    namespace :v1, &current_api_routes
  end
  scope module: :api do
    namespace :v1, &current_api_routes
  end

  if Rails.env.production?
    Sidekiq::Web.use Rack::Auth::Basic do |username, password|
      username == ENV.fetch('SIDEKIQ_USERNAME') && password == ENV.fetch('SIDEKIQ_PWD')
    end
  end
  mount Sidekiq::Web => '/sidekiq'
end
