# frozen_string_literal: true

Sequel.migration do
  change do
    create_table :days do
      primary_key :id
      Date :date, unique: true
      DateTime :created_at, null: false
    end
  end
end
