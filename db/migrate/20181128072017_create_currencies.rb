# frozen_string_literal: true

Sequel.migration do
  change do
    create_table :currencies do
      primary_key :id
      String :symbol, size: 4, null: false
      BigDecimal :ask, size: [16, 8], null: false
      BigDecimal :bid, size: [16, 8], null: false
      foreign_key :day_id, :days, index: true, null: false
      DateTime :created_at, null: false

      index %i[symbol day_id]
    end
  end
end
