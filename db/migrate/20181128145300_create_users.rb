# frozen_string_literal: true

Sequel.migration do
  change do
    run(<<-SQL.squish)
      CREATE EXTENSION IF NOT EXISTS citext;
    SQL

    create_table :users do
      primary_key :id
      String :auth_token, null: false, unique: true
      String :name
      Citext :email, null: false, unique: true

      DateTime :created_at, null: false
      DateTime :updated_at, null: false
    end
  end
end
