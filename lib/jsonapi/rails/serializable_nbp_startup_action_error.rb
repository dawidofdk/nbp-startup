# frozen_string_literal: true

module JSONAPI
  module Rails
    class SerializableNbpStartupActionError < Serializable::Error
      title do
        "Invalid data"
      end

      detail do
        @object.error
      end

      source do
        pointer 'data'
      end
    end
  end
end
