# frozen_string_literal: true

desc 'Update model annotations'
task annotate: :environment do
  model_files = Dir["#{Rails.root}/app/models/*.rb"]

  require 'sequel/annotate'
  Sequel::Annotate.annotate(model_files)
end
