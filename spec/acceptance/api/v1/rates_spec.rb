# frozen_string_literal: true

RSpec.resource 'Rates', api: true do
  include ActionDispatch::TestProcess

  route 'api/v1/rates', 'Rates' do
    get 'list' do
      before { NbpService.new.call }

      parameter :include

      with_options scope: %i[data attributes] do
        parameter :from_date, 'From date'
        parameter :till_date, 'Till date'
      end

      let(:from_date) { '2018-11-20' }
      let(:till_date) { '2018-11-23' }

      context 'when list rates' do
        let(:params) { {from_date: from_date, till_date: till_date} }

        example_request '200 Success' do
          expect(status).to eq(200)
          attrs = data_hash.first['attributes']

          expect(parsed_body).to be_present
          expect(data_hash.count).to eq 4
          expect(attrs['date']).to match '2018-11-20'
        end
      end
    end
  end
end
