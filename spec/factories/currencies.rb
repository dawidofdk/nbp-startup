# frozen_string_literal: true

FactoryBot.define do
  factory :currency do
    day
    sequence(:symbol) { |n| "sym#{n}" }
    ask { FFaker.numerify("##.####") }
    bid { FFaker.numerify("##.####") }
    created_at { FFaker::Time.datetime }
  end
end
