# frozen_string_literal: true

FactoryBot.define do
  factory :day do
    date { FFaker::Time.date }
  end
end
