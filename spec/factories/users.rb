# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    auth_token { "auth0|#{SecureRandom.hex}" }
    name { FFaker::Name.name }
    email { FFaker::Internet.unique.email }
  end
end
