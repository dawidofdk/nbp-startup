# frozen_string_literal: true

RSpec.describe GetTodaysExchangeRatesJob do
  describe '.perform' do
    context 'when day not created' do
      around do |example|
        Timecop.freeze(DateTime.parse("2018-11-28T11:01:00.000+00:00"))

        example.run

        Timecop.return
      end

      it 'should create currencies' do
        expect { described_class.perform_now }.to change(Currency, :count).by(2)
                                                                          .and change(Day, :count).by(1)
      end
    end
  end
end
