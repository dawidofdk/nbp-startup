# frozen_string_literal: true

require 'spec_helper'
ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../config/environment', __dir__)
abort('The Rails environment is running in production mode!') if Rails.env.production?

require 'rspec/rails'
require 'jsonapi/rspec'
require 'database_cleaner'
require 'vcr'
require 'rspec_api_documentation/dsl'
require 'webmock'

DatabaseCleaner.url_whitelist = ENV['DATABASE_URL']
RSpec::Matchers.define_negated_matcher :not_change, :change
Dir[Rails.root.join('spec/support/**/*.rb')].each(&method(:require))
Dir[Rails.root.join('spec/shared_contexts/**/*.rb')].each(&method(:require))

RspecApiDocumentation.configure do |config|
  config.template_path = 'lib'
  config.api_name = 'Nbp startup API'
  config.format = :api_blueprint
  config.request_headers_to_include = ['Authorization', 'Content-Type']
  config.response_headers_to_include = ['Content-Type']
  config.request_body_formatter = :json
end

Knock.setup do |config|
  config.token_audience = nil
  config.token_secret_signature_key = -> { Rails.application.secrets.secret_key_base }
end

RSpec.configure do |config|
  config.include FactoryBot::Syntax::Methods
  config.include JSONAPI::RSpec

  config.infer_spec_type_from_file_location!

  config.filter_rails_from_backtrace!

  config.include_context 'authenticated_user', :auth

  config.include Helpers
  config.include SerializerHelpers, type: :serializer

  config.before(:suite) do
    FactoryBot.to_create(&:save)
    DatabaseCleaner.strategy = :transaction
    DatabaseCleaner.clean_with(:truncation)
  end

  config.before(:each) do |example|
    case example.metadata[:type]
    when :acceptance
      header "Content-Type", "application/vnd.api+json"
    end
  end

  config.around(:each) do |example|
    DatabaseCleaner.cleaning do
      example.run
    end
  end
end
