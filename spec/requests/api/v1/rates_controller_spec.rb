# frozen_string_literal: true

RSpec.describe 'Rates Api', type: :request do
  let(:expected_attributes) { %w[date] }

  describe 'GET api/v1/rates' do
    context 'when user authenticated', :auth do
      context 'without params' do
        before { NbpService.new('usd', '2018-11-27').call }
        subject { get '/api/v1/rates', params: params, headers: auth_headers }

        let(:params) { {} }

        before { subject }

        it 'returns days' do
          expect(response).to have_http_status(:success)
          expect(parsed_body['data'].count).to eq 1
          expect(parsed_body['data'].first).to have_attributes(*expected_attributes)
          expect(parsed_body['data'].first).to have_type('days')
        end
      end

      context 'with params' do
        before { NbpService.new('usd', '2018-11-20', '2018-11-25').call }
        subject { get '/api/v1/rates', params: params, headers: auth_headers }

        let(:params) { {include: 'currencies', from_date: '2018-11-22', till_date: '2018-11-24'} }

        before { subject }

        it 'returns days' do
          expect(response).to have_http_status(:success)
          expect(parsed_body['data'].count).to eq 2
          expect(parsed_body['data'].first).to have_attributes(*expected_attributes)
          expect(parsed_body['data'].first).to have_type('days')
          expect(parsed_body['data'].first).to have_relationship(:currencies)
        end
      end
    end

    context 'when user not authenticated' do
      subject { get '/api/v1/rates' }

      before { subject }

      it 'returns http unauthorized' do
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end
end
