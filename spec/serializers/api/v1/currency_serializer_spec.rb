# frozen_string_literal: true

RSpec.describe Api::V1::CurrencySerializer, type: :serializer do
  let(:currency) { build_stubbed(:currency) }

  subject(:result) { serialize_entity(currency, class: {Currency: described_class}) }

  describe 'type' do
    it 'returns proper type' do
      expect(result.dig(:data, :type)).to eq :currencies
    end
  end

  describe 'attributes' do
    context 'when user is not current user' do
      it 'returns proper attributes' do
        expect(result.dig(:data, :attributes).keys).to match_array(
          %i[
            ask
            bid
            symbol
            createdAt
          ]
        )
      end
    end

    describe 'relationships' do
      it 'returns proper relationships' do
        expect(result.dig(:data, :relationships).keys).to contain_exactly :day
      end
    end
  end
end
