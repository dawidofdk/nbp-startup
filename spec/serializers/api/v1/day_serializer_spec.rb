# frozen_string_literal: true

RSpec.describe Api::V1::DaySerializer, type: :serializer do
  let(:day) { build_stubbed(:day) }

  subject(:result) { serialize_entity(day, class: {Day: described_class}) }

  describe 'type' do
    it 'returns proper type' do
      expect(result.dig(:data, :type)).to eq :days
    end
  end

  describe 'attributes' do
    context 'when user is not current user' do
      it 'returns proper attributes' do
        expect(result.dig(:data, :attributes).keys).to match_array(
          %i[
            date
          ]
        )
      end
    end

    describe 'relationships' do
      it 'returns proper relationships' do
        expect(result.dig(:data, :relationships).keys).to contain_exactly :currencies
      end
    end
  end
end
