# frozen_string_literal: true

require 'rails_helper'

RSpec.describe NbpService, type: :service do
  subject { described_class.new(*params).call }

  let(:params) { {} }

  context 'when no params provided' do
    context 'when day not exist' do
      it 'creates day', :aggregate_failures do
        expect { subject }.to change(Day, :count).by(1)
                                                 .and change(Currency, :count).by(1)
        expect(Currency.last.symbol).to eq 'usd'
        expect(Currency.last.day_id).to eq Day.last.id
      end
    end
  end

  context 'when provided params' do
    context 'with currency' do
      let(:params) { ['eur'] }

      it 'creates day with custom currency', :aggregate_failures do
        expect { subject }.to change(Day, :count).by(1)
                                                 .and change(Currency, :count).by(1)
        expect(Currency.last.symbol).to eq 'eur'
        expect(Currency.last.day_id).to eq Day.last.id
      end
    end

    context 'with currency and date range', :aggregate_failures do
      let(:params) { %w[eur 2018-02-27 2018-03-02] }

      it 'creates day with custom currency', :aggregate_failures do
        expect { subject }.to change(Day, :count).by(4)
                                                 .and change(Currency, :count).by(4)
        expect(Currency.last.symbol).to eq 'eur'
        expect(Currency.last.day_id).to eq Day.last.id
      end
    end

    context 'when day and currency are in db' do
      before { described_class.new(*params).call }

      it 'does not create day', :aggregate_failures do
        expect { subject }.to not_change(Day, :count)
          .and not_change(Currency, :count)
        expect(Currency.last.symbol).to eq 'usd'
        expect(Currency.last.day_id).to eq Day.last.id
      end
    end
  end
end
