# frozen_string_literal: true

RSpec.describe Users::AuthService do
  let(:service) { described_class.new }

  describe '#call' do
    subject(:call) do
      service.call(payload)
    end

    let(:payload) do
      {
        'sub' => "auth0|#{SecureRandom.hex}",
        "#{audience}/email" => FFaker::Internet.unique.email,
        "#{audience}/name" => FFaker::Name.name
      }
    end
    let(:audience) { ENV.fetch('AUTH0_AUDIENCE') }

    context 'when user persisted in database' do
      let!(:user) { create(:user, auth_token: payload['sub']) }

      context 'when persisted user is present' do
        it 'returns existed user', :aggregate_failures do
          expect(User).not_to receive(:create)

          expect(call).to eq user

          expect(User.count).to eq 1
        end
      end
    end

    context 'when user not persisted in database' do
      it 'returns new user', :aggregate_failures do
        expect(User).to receive(:create).and_call_original

        created_user = call

        expect(User.count).to eq 1
        expect(created_user).to eq User.first
      end

      context 'when payload does not contain name' do
        let(:payload) do
          {
            'sub' => "auth0|#{SecureRandom.hex}",
            "#{audience}/email" => FFaker::Internet.unique.email
          }
        end

        it 'creates a new user' do
          expect { call }.to change(User, :count).by(1)
        end
      end
    end
  end
end
